import React, { useState } from 'react';
import gql from "graphql-tag";
import { Mutation } from "react-apollo";
// import './register.css';

const ADD_USER = gql`mutation AddUser($id: String!, $name: String! , $email: String!, $password: String!) {
    addUser(id: $id, name: $name, email: $email, password: $password) {
      id
      name
      email
      password
    }
  }`;

const register = () => {
    let [userId, editUserId] = useState({ id: '' });
    let [userName, editUserName] = useState({ name: '' });
    let [userEmail, editUserEmail] = useState({ email: '' });
    let [userPassword, editUserPassword] = useState({ pwd: '' });
    const userIdHandler = (e) => {
        editUserId({ id: e.target.value });

    }
    const userNameHandler = (e) => {
        editUserName({ name: e.target.value });

    }
    const userEmailHandler = (e) => {
        editUserEmail({ email: e.target.value });

    }

    const userPasswordHandler = (e) => {
        editUserPassword({ pwd: e.target.value })

    }

    const userRegisterHandler = (e, addUser) => {
        e.preventDefault();
        addUser({ variables: { id: userId.id, name: userName.name, email: userEmail.email, password: userPassword.pwd } });
        editUserId({ id: '' });
        editUserName({ name: '' });
        editUserEmail({ email: '' });
        editUserPassword({ pwd: '' });

    }


    return <Mutation mutation={ADD_USER}>
        {(addUser, { data, loading, error }) => {
            return (<div>
                <form className="modal-content animate">
                <br />
                    <div className="container">
                        <label name="uId"><b>Emp ID : &nbsp; &nbsp; &nbsp; </b></label>
                        <input type="text" onChange={userIdHandler} placeholder="Enter user emp-Id" value={userId.id} name="userId" required></input><br/>
                        <label name="username"><b>Name  :  &nbsp; &nbsp; &nbsp;  &nbsp;  </b></label>
                        <input type="text" onChange={userNameHandler} placeholder="Enter user name" value={userName.name} name="username" required /><br/>
                        <label name="usermail"><b>Email-ID  : &nbsp; </b></label>
                        <input type="text" onChange={userEmailHandler} placeholder="Enter email id" value={userEmail.email} name="usermail" required></input><br/>
                        <label name="userpwd"><b>Password : &nbsp;</b></label>
                        <input type="password" onChange={userPasswordHandler} placeholder="Enter user password" value={userPassword.pwd} name="userpassword" required /><br />
                        <br />
                        <button onClick={(e) => {
                            userRegisterHandler(e, addUser)
                        }} type="submit">Login</button>
                        {(data && data.addUser) && <div><label name="successMsg" className="success-Msg"><b>Successfuly registered </b></label></div>}
                    </div>
                </form>
            </div>)
        }}</Mutation>
};

export default register;
