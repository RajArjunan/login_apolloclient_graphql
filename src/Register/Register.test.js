import React from 'react';
import { MockedProvider } from 'react-apollo/test-utils';
import gql from 'graphql-tag';
import Adapter from 'enzyme-adapter-react-16';
import Register from './register';


import { mount, configure } from 'enzyme';

configure({adapter: new Adapter()});

describe('Render Register Component', () => {
    it('Render Register Component', () => {

        const ADD_USER = gql`mutation AddUser($id: String!, $name: String! , $email: String!, $password: String!) {
            addUser(id: $id, name: $name, email: $email, password: $password) {
              id
              name
              email
              password
            }
          }`;
        const mockedData = [{
            request: {
              query: ADD_USER,
              variables: { id: '44', name: 'test44', email: 'test44@gmail.com', password: 'test@123' },
            },
            result: {
              data: {
                addUser: { name: 'raj'}
              },
            },
          }]


        const renderComponent = mount(<MockedProvider mocks={mockedData}><Register /></MockedProvider>);
        renderComponent.find("input").at(0).simulate('change', '');
        renderComponent.find("input").at(1).simulate('change', '');
        renderComponent.find("input").at(2).simulate('change', '');
        renderComponent.find("input").at(3).simulate('change', '');
        renderComponent.find("button").simulate("click");
    })
   
});

