import React, { useState } from 'react';
import gql from "graphql-tag";
import { Mutation } from "react-apollo";
import './style.css';

const USER_LOGIN = gql`mutation login($email: String!, $password: String!) {
        login(email: $email, password: $password){
            name
        }
    }`;

const Login = (props) => {
    const [inputVal, updateInputVal] = useState({
        userName: '',
        pwd: '',
        loginForm: true
    });
    const firstName = (event) => {
        updateInputVal({ userName: event.target.value, pwd: inputVal.pwd, loginForm: inputVal.loginForm });
    }
    const lastName = (event) => {
        updateInputVal({ pwd: event.target.value, userName: inputVal.userName, loginForm: inputVal.loginForm });
    }
    const loginHandler = (e, login) => {
        e.preventDefault();
        updateInputVal({ pwd: inputVal.pwd, userName: inputVal.userName, loginForm: false })
        login({ variables: { email: inputVal.userName, password: inputVal.pwd } });
    }

    return <Mutation mutation={USER_LOGIN}>
        {(login, { data, loading, error }) => {
            
            return (<div>
                {!(data && data.login) && <form className="modal-content animate">
                    <div className="imgcontainer">
                        <img src="https://www.colourbox.com/preview/9531609-user-flat-icon.jpg" alt="Avatar" className="avatar" />
                    </div>
                    <div className="container">
                        <label name="uname"><b>Username  : </b></label>
                        <input type="text" onChange={firstName} placeholder="Enter Username" name="uname" required></input>
                        <label name="psw"><b>Password  : </b></label>
                        <input type="password" onChange={lastName} placeholder="Enter Password" name="psw" required />
                        <button onClick={e => {
                            loginHandler(e, login)
                        }} type="submit">Login</button>
                        {(data && (data.login === null)) && <div className="login-error"><b>Username or password incorrect</b></div>}
                    </div>
                </form>}
                {((data && data.login)) && <div className="login-success"><b>You successfully logged in to this website...!!!</b></div>}
            </div>)
        }}</Mutation>
};

export default Login;
