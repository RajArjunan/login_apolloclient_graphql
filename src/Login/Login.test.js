import React from 'react';
import { MockedProvider } from 'react-apollo/test-utils';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';
import wait from 'waait';
import Login from './index';
import Adapter from 'enzyme-adapter-react-16';

import { mount, configure } from 'enzyme';

configure({adapter: new Adapter()});

describe('Render Login Component', () => {
    it('Render Login Component', () => {
        const USER_LOGIN = gql`mutation login($email: String!, $password: String!) {
            login(email: $email, password: $password){
                name
            }
        }`;
        const mockedData = [{
            request: {
              query: USER_LOGIN,
              variables: { email: 'test45@gmail.com', password: 'test@45' },
            },
            result: {
              data: {
                login: { name: 'raj'}
              },
            },
          }]


        const renderComponent = mount(<MockedProvider mocks={mockedData}><Login /></MockedProvider>);
        renderComponent.find("input").at(0).simulate('change', '');
        renderComponent.find("input").at(1).simulate('change', '');
        renderComponent.find("button").simulate("click");
    })
   
});

