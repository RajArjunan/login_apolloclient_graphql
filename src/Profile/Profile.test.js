import React from 'react';
import { MockedProvider } from 'react-apollo/test-utils';
import gql from 'graphql-tag';
import Profile from './index';
import Adapter from 'enzyme-adapter-react-16';

import { mount, configure } from 'enzyme';

configure({adapter: new Adapter()});

describe('Render Login Component', () => {
    it('Render Login Component', () => {
        const GET_USERS = gql`
        {
            users {
              id
              name
              email
            } 
        }
        `;
        const mockedData = [{
            request: {
              query: GET_USERS,
            },
            result: {
              data: {
                users: { name: 'raj'}
              },
            },
          }]


        const renderComponent = mount(<MockedProvider mocks={mockedData}><Profile /></MockedProvider>);
        console.log('renderComponent', renderComponent);
    })
   
});

