import React from 'react';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';

import './style.css';

const GET_USERS = gql`
{
    users {
      id
      name
      email
    } 
}
`;

const Profile = (props) => (
  <Query query={GET_USERS}>
    {({ data, loading }) => {
      const { users = [] } = data;
      if (loading || !data) {
        return <div>Loading ....!!!</div>;
      }

      const allUsersWrap = users.map((val, index) => {
        return (
          <div key={index}>
            <span className="firstname"><b>FirstName:</b>      </span><span className="firstname-value">{val.name}</span><br />
            <span className="lastName"><b>Email ID:</b>       </span><span className="lastName-value">{val.email}</span><br />
            <span className="photo-text"><b>Profile Photo : </b></span>
            <div className="profile-photo"><img src="https://www.colourbox.com/preview/9531609-user-flat-icon.jpg" alt="Smiley face" width="100" height="75"></img></div>
            <hr></hr>
          </div>
        )
      });

      return <div>
        <h1 className="user-title">User Profile </h1>
        <span><b>Total Profile : {users.length}</b></span> <br />
        <br />
        {allUsersWrap}
      </div>
    }}
  </Query>
);

export default Profile;
