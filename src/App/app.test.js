import React from 'react';
import App from './index';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';

configure({adapter: new Adapter()});

describe('Render App Component', () => {
    it('Render App Component', () => {
        const renderComponent = shallow(<App />);
        renderComponent.instance().displayUserList();
        renderComponent.instance().loginHandler();
        renderComponent.instance().RegisterHandler();
    })
   
});

