import React, { Component, Fragment } from 'react';
import Login from '../Login';
import Profile from '../Profile';
import Register from '../Register/register'
import './style.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isAllUser: false,
      loginPage: false,
      isRegister: false
    }
  }
  displayUserList = () => {
    this.setState({ isAllUser: !this.state.isAllUser, isRegister: false, loginPage: false });
  }
  loginHandler = () => {
    this.setState({ loginPage: !this.state.loginPage, isAllUser: false, isRegister: false });
    this.setState({ isAllUser: false });
  }
  RegisterHandler = () => {
    this.setState({ isRegister: !this.state.isRegister, loginPage: false, isAllUser: false });
  }

  render() {

    return <Fragment>
      <header>
        <h2>PHOTON INFOTECH PVT LTD</h2>
      </header>
      <section>
        <nav>
          <ul>
            <li><button onClick={this.displayUserList}>All User List</button>&nbsp;&nbsp;</li>
            <li><button onClick={this.loginHandler}>Login</button>&nbsp;&nbsp;</li>
            <li><button onClick={this.RegisterHandler}>Register</button></li>
          </ul>
        </nav>
        <article>
         <div className="vertical-scroll">
          {this.state.isAllUser && <Profile />}
          {this.state.loginPage && <Login islogin={this.state.loginPage} />}
          {this.state.isRegister && <Register />}
          </div>
        </article>
      </section>
      {/* <footer>
        <p>Company © W3docs. All rights reserved.</p>
      </footer> */}
    </Fragment>
  }
}

export default App;
