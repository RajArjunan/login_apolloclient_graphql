import React from 'react';
import ReactDOM from 'react-dom';
import { ApolloProvider } from 'react-apollo';
import ApolloClient from "apollo-boost";
// import { InMemoryCache } from 'apollo-cache-inmemory';
import * as serviceWorker from './serviceWorker';
import App from './App';
import './style.css';
// const cache = new InMemoryCache();

const client = new ApolloClient({
  uri: "http://localhost:2000/graphql"
});

// client
//   .query({
//     query: gql`
//     {
//       allUsers {
//         id
//         firstName
//         lastName
//         email
//         avatar
//       }
//     }
//     `
//   })
//   .then(result => console.log("result :::::: ", result)); 
ReactDOM.render(
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>,
  document.getElementById('root'),
);

serviceWorker.unregister();
