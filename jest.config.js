
module.exports = {
    "collectCoverage": true,
    "coverageDirectory": "coverage",
    "verbose":true,
    "roots": ["./src"],
    "moduleNameMapper": {
        "\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$": "<rootDir>/__mocks__/fileMock.js",
        "\\.(css|less|scss|sass)$": "identity-obj-proxy"
      },
    "transform": {
        "^.+||.js?$": "babel-jest"
    },
     setupFiles: [
    '<rootDir>/tests/setup.js']
}
// '<rootDir>/tests/shim.js',